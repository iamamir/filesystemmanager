package handler;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * this class will handle all the file operations
 * each method have a single and different purpose
 */
public class FileHandler {


    private List<String> fileN;
    private String directoryPath;

    public FileHandler() {
        fileN = new ArrayList<>();
        directoryPath = "C:\\Users\\Amir\\Desktop\\filesystemmanager\\src\\resources";
    }

    /**
     * it loops through, the files in the directory, and if the file is of type file, then the name of the file is added to the list of fileN
     *
     * @return a list of file names
     */
     
    public List<String> getFileN() {
        File[] files = new File(directoryPath).listFiles();
        for (File file : files) {
            if (file.isFile()) {
                fileN.add(file.getName());
            }
        }
        return fileN;
    }

    public List<String> readFile(){
        List<String> lines = new ArrayList<>();
        //buffer to read line by line
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(directoryPath + "\\log history.txt"))) {


            String line;
            // to keep iterating as long as there's line in the buffer
            while ((line = bufferedReader.readLine()) != null) {

                lines.add(line);

            }
            // returns the number of counted lines
            return lines;

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return null;

    }

    /**
     * this method will get the size of a specific file by counting the bytes
     *
     * @param fileName it takes fileName as parameter, and then targets that specific file to get it's size
     * @return once there's no more data in the file, it stops iterating and returns the actual size of the file as an integer
     */
    public int getSize(String fileName) {
        // try with resources, which will then dispose everything once it ends
        // FileInputStream to read byte by byte of a specific file
        try (FileInputStream fileInputStream = new FileInputStream(directoryPath + "/" + fileName)) {

            int data = fileInputStream.read();
            int byteCount = 0;

            // -1 indicates no more data
            while (data != -1) {

                // process data
                byteCount++;

                // next data
                data = fileInputStream.read();
            }
            return byteCount;
            // to handle any IO exception
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return 0;
        }
        //to handle any other exception in general
        catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    /**
     * to get how many numbers a file has.
     *
     * @param fileName to target a specific file
     * @return it will return the count of the lines of a file as an integer
     */
    public int getNumberOfLine(String fileName) {
        //buffer to read line by line
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(directoryPath + "/" + fileName))) {
            int lines = 0;

            String line;
            // to keep iterating as long as there's line in the buffer
            while ((line = bufferedReader.readLine()) != null) {
                // process the line
                lines++;


            }
            // returns the number of counted lines
            return lines;

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return 0;

    }

    /**
     * it searchs for a specific word in a file, and counts how many times it occurred
     *
     * @param word     the word to be searched for
     * @param fileName the file where to search for the word
     * @return it returns how many times the word occurred, otherwise a 0 is returned
     */
    public int scanForSpecificWord(String word, String fileName) {
            //scanner to read the file, and to look for each word, then space is identified as a word separator
        try (Scanner scanner = new Scanner(new File(directoryPath + "/" + fileName))) {
            int wordCounter = 0;

            scanner.useDelimiter(" ");
            String fWord;

            // as long as there's data/word to read, it will keep iterating
            while (scanner.hasNext()) {
                fWord = scanner.next();
                // it checks if the word matches the searched word
                // in case of a match, then it increments the counter
                if (fWord.contains(word)) {
                    wordCounter++;
                }
            }
            // if counter bigger than zero, means there were at least one match ands it returns that
            // otherwise, 0 is returned
            if (wordCounter > 0)
                return wordCounter;

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }


    /**
     * it logs every operation conducted by the user
     * @param log it's the message to be logged, containing what happened and how long it took in milliseconds
     */
    public void logWriter(String log) {

        // the path of the log file
        String path = directoryPath + "/log history.txt";
        // the format of the time, to pick up the current time of the operation
        String dateAndTime =  new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());

        // try with resources to automatically dispose everything afterwords, just to avoid, in case forgetting to close the filewriter afterwords
        // filewrite to write to a file, it takes the file path and true to allow writing again to the same file without removing what is already written to the file
        try (FileWriter fileWriter = new FileWriter(path, true);){

            // write method to write to the file
            fileWriter.write("On ("+dateAndTime + ") " + log + "\n");


        } catch (IOException e) {
            System.out.println();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
