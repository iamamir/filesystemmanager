package handler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * an instance of this class will be called when the application starts.
 * this class responsible for the interacting with the user showing the menu options, each menu has it's own method!
 *
 */
public class MenuHandler{

    // date is used to get the current date, and the other attributes namely, start, end and duration, is to calculate the time it took each operation in milliseconds
    private String date;
    private long start, end, duration;

    // the date is set is initialized in the constructor, to get the current date and time.
    public MenuHandler() {
        date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
    }

    /**
     *  this is the main menu, and depending on the user's choice, it will call a sub-menu!
     */
    public void mainMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean isTrue = true;
        String choice;

        // to loop through the option, and notify the user in case he didn't enter the correct value.
        // it breaks out of the loop when the user enter 3.
        while (isTrue) {
            System.out.println("+******File System Manager******+");
            System.out.println("|\t1. File manipulation");
            System.out.println("|\t2. Show log history");
            System.out.println("|\t3. Exit");
            System.out.println("+*******************************+");
            System.out.print(">:");

            choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    fileManipulationMenu();
                    break;
                case "2":
                    List<String> lines = new FileHandler().readFile();
                    for (String line: lines
                         ) {
                        System.out.println(line);
                    }
                    new FileHandler().readFile();
                    break;
                case "3":
                    System.out.println("Bye!");
                    isTrue = false;
                    break;

                default:
                    System.out.println("Please make sure your choice is between 1-3");
            }
        }
    }

    /**
     *  a sub menu to help the user manipulate the file, through the displayed options
     *
      */
    private void fileManipulationMenu() {

        Scanner scanner = new Scanner(System.in);
        boolean isTrue = true;

        // an instance of the FileHandler class to use its methods
        FileHandler fileHandler = new FileHandler();
        // a list of file names
        List<String> files = new FileHandler().getFileN();

        String choice;

        while (isTrue) {
            System.out.println("+*********File Manipulation*********+");
            System.out.println("|\t1. List all files");
            System.out.println("|\t2. List specific files");
            System.out.println("|\t3. Manipulate specific txt file");
            System.out.println("|\t4. Back to main menu");
            System.out.println("+***********************************+");
            System.out.print(">:");

            choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    // this will go through the list of names and display them one by one as well as the index of each file name.
                    start = System.currentTimeMillis();
                    for (String fileName : files) {
                        System.out.println(files.indexOf(fileName) + "- " + fileName);
                    }
                    end = System.currentTimeMillis();
                    duration = end - start;

                    //to log the action in the log history file
                    fileHandler.logWriter("you wished to see a list of all the files: " + files.toString() + ". It took " + duration + "ms");

                    break;
                case "2":

                    System.out.print("Please enter the file extension: ");
                    String fExtension = scanner.nextLine();
                    //this list will be used as a temp list of the main list to add the specific files needed.
                    List<String> specFiles = new ArrayList<>();

                    start = System.currentTimeMillis();

                    //it will iterate through the main list of files and check the extension of each file, if the file name has an extension,
                    // in the case it has an extension, then it will be put in the extension variable and then compared, to the extension the user wrote, if it is the same,
                    // then the file name will be added to the specific list and displayed to the user along with other files share the same extension
                    for (String fName : files) {
                        String extension = "";

                        int i = fName.lastIndexOf('.');
                        int p = Math.max(fName.lastIndexOf('/'), fName.lastIndexOf('\\'));

                        if (i > p) {
                            extension = fName.substring(i + 1);
                        }

                        if (extension.equalsIgnoreCase(fExtension))
                            specFiles.add(fName);
                    }
                    end = System.currentTimeMillis();

                    duration = end - start;
                    System.out.print("List of file(s) with " + fExtension + " as extension: " + specFiles.toString() +"\n");

                    fileHandler.logWriter("you wished to search for a list of files with (" + fExtension + ") extension, " + specFiles.toString() + ". It took " + duration + "ms");

                    break;
                case "3":
                    // this to make sure that the targeted file already exist, the user won't enter a number that is bigger than the size of the list.
                    // if the user enters a correct number then, s/he will be moved to the next sub menu
                    System.out.println("Please enter the index of the targeted file between(0 - " + (files.size() - 1) + "): ");
                    int fileIndex = scanner.nextInt();
                    scanner.nextLine();
                    if (fileIndex < files.size())
                        manipulateSpecificFile(fileIndex); // this should take the file name/or the index
                    else System.out.println("Try again!");
                    break;
                case "4":
                    // to break the loop and go back to the main menu
                    isTrue = false;
                    break;

                default:
                    System.out.println("Please make sure your choice is between 1-4");
            }
        }
    }

    /**
     * Yet another sub menu to give further specifications of a certain file
     * the file index is passed in as a parameter, which is then used to easily get that file's name
     *
     * @param fileIndex
     */
    private void manipulateSpecificFile(int fileIndex) {
        Scanner scanner = new Scanner(System.in);
        boolean isTrue = true;

        FileHandler fileHandler = new FileHandler();
        List<String> files = fileHandler.getFileN();
        String choice;

        // to get the extension of the file, in order to apply a condition checker in the last 2 options, because they can be used on txt files only.
        String extension = "";
        int i = files.get(fileIndex).lastIndexOf('.');
        int p = Math.max(files.get(fileIndex).lastIndexOf('/'), files.get(fileIndex).lastIndexOf('\\'));

        if (i > p) {
            extension = files.get(fileIndex).substring(i + 1);
        }

        while (isTrue) {
            System.out.println("+********************Manipulate Specific File*******************+");
            System.out.println("|\t1. Show File Name");
            System.out.println("|\t2. Show File Size");
            System.out.println("|\t3. Show number of lines the file has");
            System.out.println("|\t4. Search for specific word and how many times it appeared");
            System.out.println("|\t5. Go back");
            System.out.println("+***************************************************************+");
            System.out.print(">:");


            choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    long start = System.currentTimeMillis();
                    // this gets the file name from the list using the index.
                    String fileName = files.get(fileIndex);
                    long end = System.currentTimeMillis();
                    duration = end - start;

                    System.out.println("The name of the file is: "+fileName);

                    fileHandler.logWriter("you wished to see the name of the file, which is: (" + fileName + "). It took " + duration + "ms");

                    break;

                case "2":
                    start = System.currentTimeMillis();
                    // it gets the size of the file by calling the get size method in the filehandler class
                    int fileSize = fileHandler.getSize(files.get(fileIndex));
                    end = System.currentTimeMillis();
                    duration = end - start;
                    System.out.println("The file size is: " + fileSize + " bytes, or  " + fileSize / 1024.0 + " kilobytes (" + (int) Math.ceil(fileSize / 1024.0) + "KB on disk)");

                    fileHandler.logWriter("you wished to see the size of the file which is " + fileSize + " bytes, or " +
                            fileSize / 1024.0 + " kilobytes (" + (int) Math.ceil(fileSize / 1024.0) + "KB on disk). It took " + duration + "ms");
                    break;
                case "3":
                    // it checks if the file, is a txt file,
                    // in the case of txt files, the user could check how many lines the file has
                    if (extension.equalsIgnoreCase("txt")) {
                        start = System.currentTimeMillis();
                        // it gets the number of lines in the file by calling the get size method in the filehandler class
                        int numberOfLines = fileHandler.getNumberOfLine(files.get(fileIndex));
                        end = System.currentTimeMillis();
                        duration = end - start;
                        System.out.println("The number of lines are: "+numberOfLines);
                        fileHandler.logWriter("you wished to see the number of lines and they are (" + numberOfLines + ") line. It took " + duration + "ms");

                    } else {
                        System.out.println("You can only do this for txt files!");
                    }
                    break;
                case "4":
                    // it checks if the file, is a txt file,
                    // in the case of txt files, the user could check how many lines the file has
                    if (extension.equalsIgnoreCase("txt")) {
                        System.out.print("Please write a word to search for: ");
                        String word = scanner.nextLine();

                        start = System.currentTimeMillis();
                        // it searches for a specific word in the file and how many times it appeared, by calling the get size method in the filehandler class
                        int occurrences = fileHandler.scanForSpecificWord(word, files.get(fileIndex));
                        end = System.currentTimeMillis();

                        duration = end - start;
                        // in case the word exist, then the occurrences will be a number bigger than a zero, in that case it will show the word and how many times it occurred
                        // otherwise, a messege is displayed telling the user that this word does not exist in the file.
                        if (occurrences > 0) {
                            System.out.println("The word ("+word+")exists and appeared: "+occurrences);
                            fileHandler.logWriter("you searched for this word: " + word + " and it appeared: " + occurrences + ". It took " + duration + "ms");
                        }else {
                            System.out.println("The word ("+word+")does not exist in the file");
                            fileHandler.logWriter("you searched for this word: " + word + " and it doesn't exist in the file . It took " + duration + "ms");
                        }
                    } else {
                        System.out.println("You can only do this for txt files!");
                    }
                    break;

                case "5":
                    // to go back and out of this sub-menu
                    isTrue = false;
                    break;

                default:
                    System.out.println("Please make sure your choice is between 1-5");
            }

        }
    }


}
