# FileSystemManager 
## FSM is a console application that manipulates and manages files.
---
### The application starts by showing the user a menu and prompts the user to choose from this menu, depending on the user choices, the application will execute the choosen operation.
 1. The user can list all the files in a folder.
 2. The user could list a list of files of a specific type.
 3. The user could see specifications of a certain type such as:
* File name
* The size of the file
* Number of lines a file has (only txt files)
* Search for a specific word and how many times it occured (only txt files).
### Futhermore, the application logs every executed operation and saves it in a log history file, which could be read by the user.
---
## Error handling have been implemented to prevent, errors from crashing the application while run time.
---
### Below are the used comands to compile and create the executable jar file of the program.
![Screenshot](https://gitlab.com/iamamir/filesystemmanager/-/raw/master/Commands.PNG)

